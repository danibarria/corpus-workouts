import styled from "styled-components";

export const Header = styled.nav`
    background-color: var(--black);
    height: 48px;
    width: 100%;
    display: flex;
    justify-content: center;
`

export const Footer = styled.footer`
    background-color: var(--black);
    width: 100%;
    padding: 16px 6px;
` 

export const Container = styled.div`
    width: 100%;
    background-color: var(--white);
`

export const SmallText = styled.p<{ color: string }>`
    margin: 0;
    font-size: 14px;
    font-family: sans-serif;

    color: ${props => props.color && `var(--${props.color})`};
`

export const MediumText = styled.p<{ color: string }>`
    margin: 0;
    font-size: 18px;
    font-family: sans-serif;

    color: ${props => props.color && `var(--${props.color})`};
`

export const LargeText = styled.strong<{ color: string }>`
    margin: 0;
    font-size: 22px;
    font-family: sans-serif;
    
    color: ${props => props.color && `var(--${props.color})`};
`