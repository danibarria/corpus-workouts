import styled from "styled-components";

export const Content = styled.main`
    width: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;

    text-align: center;

    .landing-map {
        margin: 10px 0 2px;
    }
`


export const Activities = styled.figure`
    width: 100%;
    display: flex;
    flex-wrap: wrap;
    justify-content: center;

    column-gap: 12px;
    row-gap: 6px;
`