import type { NextPage } from 'next'
import Head from 'next/head'
import Image from 'next/image'
import * as SC from '../components/Shared'
import * as Landing from '../components/Landing'

const Home: NextPage = () => {
  return (
    <SC.Container>
      <Head>
        <title>Gimnasio Corpus</title>
        <meta name="description" content="Unite a corpus" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <SC.Header>
        <Image src="/images/logo.svg" width={185} height={48} alt="logo corpus" />
      </SC.Header>

      <Landing.Content>
        <Image src="/images/landing/banner.svg" width={375} height={225} alt="Somos corpus, somos familia" />
        <SC.LargeText color='black'>Salas en simultáneo</SC.LargeText>
        <Landing.Activities>
          <Image src="/images/landing/box.svg" width={104} height={60} alt="actividad Box" />
          <Image src="/images/landing/musculacion.svg" width={104} height={60} alt="actividad musculación" />
          <Image src="/images/landing/pilates.svg" width={104} height={60} alt="actividad pilates" />
          <Image src="/images/landing/funcional.svg" width={104} height={60} alt="actividad funcional" />
          <Image src="/images/landing/zumba.svg" width={104} height={60} alt="actividad zumba" />
        </Landing.Activities>

        <SC.MediumText color='black'>Soberanía Nacional 25, Trelew-Chubut</SC.MediumText>
        <Image className='landing-map' src="/images/landing/map.svg" width={335} height={120} alt="Mapa de ubicación" />
        <SC.SmallText color='black'>Lunes a viernes 07:00 - 22:00hs</SC.SmallText>
        <SC.SmallText color='black'>Sábados 10:00 - 14:00 y 17:00 - 20:30hs</SC.SmallText>
      </Landing.Content>
      
      <SC.Footer>
        footer
      </SC.Footer>
    </SC.Container>
  )
}

export default Home
